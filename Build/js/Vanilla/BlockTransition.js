// Classe BlockTransition
// classe de gestion des transitions
const BlockTransition = {
  // function new()
  // initialise la Classe
  // return void
  new : function(){
    var index = 0
    // recuperation des wrapper
    this.wrapper = document.getElementsByClassName("bt-wrapper")
    // initialisationd es pages
    for (index = 0; index < this.wrapper.length; index++) {
      this.wrapper[index].firstElementChild.classList.add("bt-current")
    }
    // mise en place des switch
    var cmdSwitch = document.getElementsByClassName("bt-switch")
    for (index = 0; index < cmdSwitch.length; index++) {
      cmdSwitch[index].addEventListener("click", function(e){
        var wrapper = undefined
        var currPage = -1
        var newPage = -1
        if(this.classList.contains("bt-wrapper")) {
          wrapper = this
        }
        else {
          var idWrapper = this.attributes["bt-wrapper"].value
          var btTo = this.attributes["bt-to"].value
          if (idWrapper != undefined) {
            wrapper = document.getElementsById(idWrapper)
          }
          else {
            // si le wrapper est incompler (à modifier par une erreur avec
              // le gestionaaire d'erreur)
              window.console.log(this)
          }
          if (btTo != undefined) {
            newPage = parseInt(btTo);
          }
        }
        if(wrapper != undefined){
          var delay = this.attributes["bt-delay"].value
          var btIn = this.attributes["bt-in"].value
          var btOut = this.attributes["bt-out"].value
          var btCurrent = wrapper.getElementsByClassName("bt-page")
          for (var btIndex = 0; btIndex < btCurrent.length; btIndex++){
            if (btCurrent[btIndex].classList.contains("bt-current")) {
              currPage = btIndex
            }
          }
          if (newPage < 0) {
            newPage = currPage + 1
          }
          if (newPage >= wrapper.getElementsByClassName("bt-page").length) {
            newPage = 0
          }
          if (currPage == newPage) {
            return true;
          }
          if (currPage > newPage) {
            var reverseIn = undefined;
            if (this.attributes["bt-reverse-in"] != undefined) {
              reverseIn = this.attributes["bt-reverse-in"].value;
            }
            var reverseOut = undefined;
            if(this.attributes["bt-reverse-out"]) {
              reverseOut = this.attributes["bt-reverse-out"].value;
            }
            if (reverseIn != undefined) {
              btIn = reverseIn;
            }
            if (reverseOut != undefined) {
              btOut = reverseOut;
            }
          }
          BlockTransition.switch(
            wrapper,
            currPage,
            newPage,
            btIn,
            btOut,
            delay
          )
        }
        e.stopPropagation()
      })
    }
  },

  // function switch(HTMLElement wrapper, int currPage, int newPage, String animIn
  //   ,String animOut, int delay)
  // change la page courante avec les animations données
  // wrapper
  // conteneur des pages
  // newPage
  // index de la nouvelle page
  // animIn
  // animation de la page entrante
  // animOut
  // animation de la page sortante
  // delay
  // durée de l'animation
  switch : function(wrapper, currPage, newPage, animIn, animOut, delay){
    var pageList = wrapper.getElementsByClassName("bt-page")
    var newPageElement = pageList[newPage]
    var currPageElement = pageList[currPage]
    newPageElement.style.animation = animIn + " " + delay + "ms"
    currPageElement.style.animation = animOut + " " + delay + "ms"
    newPageElement.classList.add("bt-current")
    setTimeout(function(){
      currPageElement.classList.remove("bt-current")
      currPageElement.style.animation = ""
      newPageElement.style.animation = ""
    },
    delay)
  }
}

window.addEventListener("DOMContentLoaded", function(){
  BlockTransition.new()
})
