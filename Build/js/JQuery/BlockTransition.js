// Classe BlockTransition
// Classe de gestion des transitions
const BlockTransition = {
  // function new()
  // initialise la classe
  // return void
  new : function(){
    //recuperation des wrapper
    this.wrapper = $(".bt-wrapper")
    // initalisation des pages
    this.wrapper.each(function(index, element){
      $(element).children(".bt-page").first().addClass("bt-current")
    })
    // mise en place des switch
    $(".bt-switch").on("click", function(e){
      var wrapper = undefined
      var currPage = -1
      var newPage = -1
      // si le switch est la wrapper
      if ($(this).hasClass("bt-wrapper")) {
        wrapper = $(this)
      }
      else {
        var idWrapper = $(this).attr("bt-wrapper")
        var btTo = $(this).attr("bt-to")
        if (idWrapper != undefined) {
          wrapper = $("#"+idWrapper)
        }
        else {
          // si le wrapper est incompler (à modifier par une erreur avec
          // le gestionaaire d'erreur)
          window.console.log(this)
        }
        if (btTo != undefined) {
          newPage = parseInt(btTo)
        }
      }
      if (wrapper != undefined) {
        var delay = $(this).attr("bt-delay")
        var btIn = $(this).attr("bt-in")
        var btOut = $(this).attr("bt-out")
        currPage = wrapper.children(".bt-page").index(wrapper.children(".bt-current"))
        if (newPage < 0) {
          newPage = currPage + 1
        }
        if (newPage >= wrapper.children(".bt-page").length) {
          newPage = 0
        }
        if (currPage == newPage) {
          return true;
        }
        if (currPage > newPage) {
          var reverseIn = $(this).attr("bt-reverse-in")
          var reverseOut = $(this).attr("bt-reverse-out")
          if (reverseIn != undefined) {
            btIn = reverseIn;
          }
          if (reverseOut != undefined) {
            btOut = reverseOut;
          }
        }
        BlockTransition.switch(
          wrapper,
          newPage,
          btIn,
          btOut,
          delay
        )
      }
      e.stopPropagation()
    })
  },

  // function switch(JQuery wrapper, int currPage, int newPage, String animIn
  //   ,String animOut, int delay)
  // change la page courante avec les animations données
  // wrapper
  // conteneur des pages
  // newPage
  // index de la nouvelle page
  // animIn
  // animation de la page entrante
  // animOut
  // animation de la page sortante
  // delay
  // durée de l'animation
  switch : function(wrapper, newPage, animIn, animOut, delay){
    //var animDelay = delay / 2
    var animDelay = delay
    var jqNewPage = $(wrapper.children(".bt-page").get(newPage))
    var jqCurrPage = wrapper.children(".bt-page.bt-current")
    jqCurrPage.css("animation", animOut + " " + animDelay + "ms")
    jqNewPage.css("animation", animIn + " " + animDelay + "ms")
    jqNewPage.addClass("bt-current")
    setTimeout(function(){
      jqCurrPage.removeClass("bt-current")
      jqCurrPage.css("animation", "")
      jqNewPage.css("animation", "")
      //setTimeout(function(){
      //},
      //animDelay)
    },
    animDelay)
  },

  // function keyframeExist(keyframeName)
  // detecte la présence d'une keyframe
  keyframeExist : function(keyframeName) {
    var retour = false
    for (var cssDoc of document.styleSheets){
      for (var cssRule of cssDoc.cssRules){
        if (cssRule instanceof CSSKeyframesRule){
          if (cssRule.name == keyframeName){
            retour = true
          }
        }
      }
    }
    return retour
  }
}

$(window).on("load", function(){
  BlockTransition.new()
})
