# BlockTransition



## Description
Block Transition est un module de gestion des transitions sour les pages web.

## Utilisation
### Activer Block Transition
Ajouter le fichier BlockTransition.js correspondant à votre framework javascript et le fichier BlockTransition.css

il est possible de le récupérer en copiant les fichiers depuis le repository, mais je conseille d'utiliser depuis le CDN (les exemples de code utiliseront la version CDN).

### Appliquer une transition
Pour appliquer une transition, créer un div avec la classe bt-wrapper ce seras le conteneur des page à faire transitionner.
Pour les pages dans le conteneur, utiliser la classe bt-page.

L'actionneur des transitions utilise la classe bt-switch
L'attribut de l'actionneur bt-in est pour la transition entrante.
L'attribut de l'actionneur bt-out est pour la transition sortante.
L'attribut bt-delay indique la durée de la transition en milliseconde

Exemple de code avec le conteneur comme actionneur :

    <div class="bt-wrapper bt-switch" bt-in="bt-zoom-in" bt-out="bt-zoom-out" bt-delay="1000">
      <div class="bt-page">
       Page 1
      </div>
      <div class="bt-page">
        Page 2
      </div>
      <div class="bt-page">
        Page 3
      </div>
    </div>

Si la transition part d'une page supérieur à la nouvelle page, il est possible d'appliquer une transition différente.
L'attribut de l'actionneur bt-in-reverse est pour la transition inverse entrante.
L'attribut de l'actionneur bt-out-reverse est pour la transition inverse sortante.

Exemple de code avec une transition inverse :

    <div class="bt-wrapper bt-switch" bt-in="bt-rotate-slide-left-in" bt-out="bt-rotate-slide-left-out" bt-in-reverse="bt-rotate-slide-right-in" bt-out-reverse="bt-rotate-slide-right-out" bt-delay="1000">
      <div class="bt-page">
       Page 1
      </div>
      <div class="bt-page">
        Page 2
      </div>
      <div class="bt-page">
        Page 3
      </div>
    </div>

Si l'actionneur n'est pas le conteneur, l'actionneur doit avoir l'attribut bt-wrapper avec l'id du conteneur

Exemple de code avec un bouton comme actionneur :

    <button type="button" class="bt-switch" bt-in="bt-zoom-in" bt-out="bt-zoom-out" bt-wrapper="wrapper" bt-delay="1000">
      Suivant
    </button>
    <div class="bt-wrapper" id="wrapper">
      <div class="bt-page">
        page 1
      </div>
      <div class="bt-page">
        page 2
      </div>
      <div class="bt-page">
        page 3
      </div>

il est possible d'indiquer la page vers laquelle on fait la transition avec bt-to.
l'index commence à 0

Exemple de code avec une liste pour transitionner vers les différentes pages

    <ul>
      <li class="bt-switch" bt-in="bt-zoom-in" bt-out="bt-zoom-out" bt-wrapper="wrapper" bt-delay="1000" bt-to="0">
        page 1
      </li>
      <li class="bt-switch" bt-in="bt-zoom-in" bt-out="bt-zoom-out" bt-wrapper="wrapper" bt-delay="1000" bt-to="1">
        page 2
      </li>
      <li class="bt-switch" bt-in="bt-zoom-in" bt-out="bt-zoom-out" bt-wrapper="wrapper" bt-delay="1000" bt-to="2">
        page 3
      </li>
    </ul>
    <div class="bt-wrapper" id="wrapper">
      <div class="bt-page">
        page 1
      </div>
      <div class="bt-page">
        page 2
      </div>
      <div class="bt-page">
        page 3
      </div>
    </div>

Il est possible d'avoir des sous pages :

    <div class="bt-wrapper bt-switch" bt-in="bt-zoom-in" bt-out="bt-zoom-out" bt-delay="1000">
      <div class="bt-page">
       Page 1
      </div>
      <div class="bt-page">
        <div class="bt-wrapper bt-switch" bt-in="bt-zoom-in" bt-out="bt-zoom-out" bt-delay="1000">
          <div class="bt-page">
            page 2-1
          </div>
          <div class="bt-page">
            page 2-2
          </div>
        </div>
      </div>
      <div class="bt-page">
        Page 3
      </div>
    </div>

## Liens CDN

- [CSS](https://cdn.statically.io/gl/xaratstudios/blocktransition/main/Build/css/BlockTransition.css)
- [JQuery](https://cdn.statically.io/gl/xaratstudios/blocktransition/main/Build/js/JQuery/BlockTransition.js)
